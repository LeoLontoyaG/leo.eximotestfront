import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environments'
import { BaseResponse } from '../../Models/baseresponse.model';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {

  private apiUrl = environment.baseurl+'Persons/';

  constructor(private http: HttpClient) { }

  getEmpleadosByEmpresaid(id: string) { 
    return this.http.get<BaseResponse>(`${this.apiUrl}getEmpleadosByEmpresaid?id=${id}`); 
  }
}
