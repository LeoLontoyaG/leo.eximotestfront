import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseResponse } from '../../Models/baseresponse.model';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environments'
import { Form } from '../../Models/Forms/form.model';
@Injectable({
  providedIn: 'root'
})
export class FormsService {

  private apiUrl = environment.baseurl+'Forms/';
  private apiUrlTypes = environment.baseurl+'FormTypes/';


  constructor(private http: HttpClient) { }


  createform(body:Form){
    return this.http.post(this.apiUrl+"add",body)
  }

  editForm(body:Form){
    return this.http.post<Form>(`${this.apiUrl}EditForm`, body);
  }

  getformbyid(id:string){
    return this.http.get<BaseResponse>(`${this.apiUrl}getformbyid?id=${id}`)
  }

  getForms(): Observable<BaseResponse> {
    return this.http.get<BaseResponse>(this.apiUrl+'getAllActivos');
  }

  getAllformtypes():Observable<BaseResponse>{
    return this.http.get<BaseResponse>(this.apiUrlTypes+'getAllTypes');
  }

  deleteById(id:string):Observable<BaseResponse>{
    return this.http.delete<BaseResponse>(`${this.apiUrl}deleteById?id=${id}`);
  }
}
