import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environments'
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../../Models/baseresponse.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {
  private apiUrl = environment.baseurl+'Companies/';

  constructor(private http: HttpClient) { }

  getallcompanies(): Observable<BaseResponse> {
    return this.http.get<BaseResponse>(this.apiUrl+'all');
  }


}
