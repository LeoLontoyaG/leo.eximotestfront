export class BaseResponse {
    StatusCode: number;
    Message: string;
    StackTrace: string;
    result: any
    results: any[];
    
    constructor() {
        this.StatusCode = 0;
        this.Message = '';
        this.StackTrace = '';
        this.results = [];
    }
}
