import { BaseEntity } from "../Base/base.model";

export class Formtype extends BaseEntity {
    description: string;
    
    constructor() {
        super();
        this.description = '';
    }
}
