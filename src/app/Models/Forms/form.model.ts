import { BaseEntity } from "../Base/base.model";

export class Form extends BaseEntity{
    name: string;
    description: string;
    personId: string;
    formTypeId: string
    priority: number;
    active: boolean;
    constructor(){
        super();
        this.name="";
        this.description="";
        this.personId="";
        this.formTypeId="";
        this.priority=1;
        this.active=true
    }
}