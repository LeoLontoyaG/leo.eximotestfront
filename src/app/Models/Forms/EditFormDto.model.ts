import { Form } from "./form.model";

export class EditFormDto extends Form{
    companyId:string
    formTypeDescription:string
    constructor(){
        super();
        this.companyId="";
        this.formTypeDescription="";
    }
}