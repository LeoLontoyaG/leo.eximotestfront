import { Form } from "./form.model";

export class formActiveDto extends Form{
    formTypeDescription:string
    personName: string
    constructor(){
        super();
        this.formTypeDescription="";
        this.personName="";
    }
}