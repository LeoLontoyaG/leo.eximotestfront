export class BaseEntity{
    id:string
    modificationDate: Date|null
    creationDate: Date
    isdeleted: boolean
    constructor(){
        this.id="";
        this.creationDate=new Date();
        this.modificationDate=null;
        this.isdeleted= false;
    }
}