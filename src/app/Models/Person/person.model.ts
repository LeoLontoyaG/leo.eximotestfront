import { BaseEntity } from "../Base/base.model";

export class Person extends BaseEntity{
    name: string;
    email?: string;
    phone?: string;
    companyId: string;
    constructor() {
        super();
      this.name = "";
      this.email = "";
      this.phone = "";
      this.companyId = "";
    }
  }
  
