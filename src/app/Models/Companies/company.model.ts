import { BaseEntity } from "../Base/base.model";

export class Company extends BaseEntity{
    name: string;
    address: string;
    constructor(){
        super();
        this.name="";
        this.address="";
    }
}