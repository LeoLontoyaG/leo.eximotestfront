import { Component, OnInit } from '@angular/core';
import { FormsService } from '../../Services/Forms/forms.service';
import { Router } from '@angular/router';
import { formActiveDto } from '../../Models/Forms/formActiveDto';
import Swal from 'sweetalert2'
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {
  constructor(private formservice: FormsService,private router:Router,private spinner: NgxSpinnerService){
  }
  results: formActiveDto[]=[]

  ngOnInit(): void {
    this.loadforms()
  }

  loadforms(){
    this.spinner.show();
    this.formservice.getForms().subscribe((res)=>{
      this.results=res.results;
      this.results.sort((a,b)=>a.priority-b.priority);
      this.spinner.hide()
    },
    error=>{
      this.spinner.hide();
      Swal.fire({
        title: "Error obteniendo informacion",
        heightAuto:false,
        text: "Contactese con un administrador",
        icon: "error"
      });
    }
  );
  }

  goto(){
    this.router.navigate(["form"])
  }

  edit(id:string){
    this.router.navigate(['/form'], { queryParams: { id: id } });
  }

  delete(id:string){
    Swal.fire({
      title: "Desactivar Tarea?",
      text: "Esto no se puede revertir!",
      heightAuto:false,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Si, Desactivalo!"
    }).then((result) => {
      if (result.isConfirmed) {
        this.formservice.deleteById(id).subscribe((res)=>{
          Swal.fire({
            title: "Desactivado!",
            heightAuto:false,
            text: "La tarea se desactivo.",
            icon: "success"
          });

          this.loadforms();
        })


      }
    });
  }

}
