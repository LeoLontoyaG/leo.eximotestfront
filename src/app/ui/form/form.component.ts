import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsService } from '../../Services/Forms/forms.service';
import { Formtype } from '../../Models/FormTypes/formtype.model';
import { CompaniesService } from '../../Services/Companies/companies.service';
import { Company } from '../../Models/Companies/company.model';
import { PersonsService } from '../../Services/Persons/persons.service';
import { Person } from '../../Models/Person/person.model';
import { Form } from '../../Models/Forms/form.model';
import { v4 as uuidv4 } from 'uuid';
import { ActivatedRoute, Router } from '@angular/router';
import { EditFormDto } from '../../Models/Forms/EditFormDto.model';
import Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrl: './form.component.scss'
})
export class FormComponent implements OnInit {
  tareaForm: FormGroup;

  constructor(private fb: FormBuilder
    ,private formservice: FormsService
    ,private companieservice:CompaniesService,
    private route: ActivatedRoute,
    private router:Router,
    private spinner: NgxSpinnerService
    ,private personService:PersonsService) 
  { 
    this.route.queryParams.subscribe(async params => {
      const id = params['id'];
      if (id) {
        this.editmode=true;
        await this.getForm(id);
      }
    });
    this.tareaForm=this.generateform();
  }
  formtypes : Formtype[]=[];
  companies: Company[]=[];
  persons: Person[]=[];
  externo=false;
  editmode: boolean|null=false;
  formtoedit=new EditFormDto();

  ngOnInit(): void {
    this.loadformtypes();
    this.loadempresas();
  }

  getlocalpersons(){
    this.companies.forEach(company => {

        if(company.name=="EXIMO"){
          console.log("CARGO EMPLEADOS",company.id);
          
           this.loadempleados(company.id);
           return;
        }
    });
  }

  loadformtypes(){
    this.spinner.show();
    this.formservice.getAllformtypes().subscribe((res)=>{
      this.formtypes=res.results;
      this.spinner.hide();
    },
    error=>{
      this.spinner.hide();
      Swal.fire({
        title: "Error obteniendo informacion",
        heightAuto:false,
        text: "Contactese con un administrador",
        icon: "error"
      });
    }
  );
  }

  generateform(): FormGroup{
    const FormGroup = this.fb.group({
      nombreTarea: ['', Validators.required],
      descripcion: ['', Validators.required],
      tipoTarea: ['', Validators.required],
      empresa:[''],
      persona: ['', Validators.required],
      prioridad: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
    });
    return FormGroup
  }

  saveForm(){
    var form=new Form();
    form.name=this.tareaForm.get('nombreTarea')?.value;
    form.description=this.tareaForm.get('descripcion')?.value;
    form.personId=this.tareaForm.get('persona')?.value;
    form.formTypeId=this.tareaForm.get('tipoTarea')?.value;
    form.priority=this.tareaForm.get('prioridad')?.value;
    form.modificationDate=null;
    if(this.editmode==null){
      form.id=this.formtoedit.id;
      form.modificationDate=new Date();
      this.formservice.editForm(form).subscribe((res)=>{
        Swal.fire({
          icon: "success",
          title: "Se edito correctamente la tarea!",
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate(["home"])
      })
    }
    else{
      form.id=uuidv4();
      this.formservice.createform(form).subscribe((res)=>{
        Swal.fire({
          icon: "success",
          title: "Se guardo la tarea!",
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate(["home"])
  
      })
    }

  }
  loadMetodo(){
    this.tareaForm.patchValue({
      nombreTarea: this.formtoedit.name,
      descripcion: this.formtoedit.description,
      tipoTarea: this.formtoedit.formTypeId,
      empresa:this.formtoedit.companyId,
      persona:this.formtoedit.personId,
      prioridad:this.formtoedit.priority
    });
  }

  loadempleados(empresaid:string){
    this.personService.getEmpleadosByEmpresaid(empresaid).subscribe((res)=>{
      this.persons=res.results;

      console.log(this.persons)

      if(this.editmode){//si esta editando carga todos los datos en los forms, una vez se cargue todos los selectores
        this.loadMetodo();
        this.editmode=null;
      }
    })
  }


  loadempresas(){
    this.persons=[];
    this.companies=[];
      this.companieservice.getallcompanies().subscribe((res)=>{
        this.companies=res.results;

        if(this.editmode){
          this.loadempleados(this.formtoedit.companyId)
        }
        else{
          this.formtypes.forEach(element => {
            if(element.id == this.tareaForm.get('tipoTarea')?.value){
              if(element.description=="Gestion externa"){
                this.companies= this.companies.filter(company => company.name !== "EXIMO");

                this.loadempleados(this.companies[0].id);
                this.externo=true; 
              }
              else{
                this.externo=false;                            
                this.getlocalpersons(); //no puede cargar las personas por que no hay compañias cargadas todavia
              }
            }
          });
        }




      })
  }


  async getForm(id:string){
    this.formservice.getformbyid(id).subscribe((res)=>{
      this.formtoedit=res.result;
    })
  }



}
